import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.Sound;

public class TestSekunde {

	public static void main(String[] args) {
		// Robi soll genau eine Sekunde geradeaus fahren
		
		
		Sound.beep();
		Button.ENTER.waitForPressAndRelease();
		LCD.drawString("Los geht�s!", 0, 0);
		
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		
		Motor.A.forward();
		Motor.B.forward();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		LCD.drawString("War das", 2, 1);
		LCD.drawString("eine Sekunde?", 2, 2);
		}
		Motor.A.stop(true);
		Motor.B.stop();
	}

}
