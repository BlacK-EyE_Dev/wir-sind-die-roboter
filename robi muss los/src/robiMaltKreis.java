import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;

public class robiMaltKreis {

	public static void main(String[] args) {
		LCD.drawString("Huii", 2, 2);
		
		Button.ENTER.waitForPressAndRelease();
		
		LCD.refresh();
		
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(180);
		
		Motor.A.forward();
		Motor.C.forward();
		
		try {
			Thread.sleep(7900);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Motor.A.stop(true);
		Motor.C.stop();

	}
}
