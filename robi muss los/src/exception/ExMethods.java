package exception;

import NVT.SensorFahrzeug;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class ExMethods extends SensorFahrzeug{
	
	SensorPort port1 = SensorPort.S1;
	private LightSensor lightSensor = new LightSensor(port1);
	private Object lichtSensor;
	
	SensorFahrzeug f = new SensorFahrzeug();

	
	public boolean pruefeAufSchlucht()  {
		boolean istSchlucht = false;
		if(lightSensor.readValue() <= 40) {
			istSchlucht = true;
		}
		return istSchlucht;
	}
	
	public void doexception() throws InterruptedException {
		
		while(true) {
			
			f.fahreVorwaertsStrecke(20);
			if(pruefeAufSchlucht() == true) {
				notsuicide();
			}
			//f.fahreRueckwaertsStrecke(10);
			for(int i = 0; i <= 9;i++) {
				f.dreheLinksGrad(10);
				
				if(pruefeAufSchlucht() == true) {
					notsuicide();
					}
			}
			
			for(int i = 0; i <= 18;i++) {
				f.dreheRechtsGrad(10);
				
				if(pruefeAufSchlucht() == true) {
					notsuicide();
					}
			}
			
			for(int i = 0; i <= 9;i++) {
				f.dreheLinksGrad(10);
				
				if(pruefeAufSchlucht() == true) {
					notsuicide();
					}
			}
		}
		
//		try {
//			pruefeAufSchlucht();
//			f.fahreVorwaerts();
//			
//			
//		    
//		} catch (InterruptedException b){  
//			f.fahreRueckwaertsStrecke(10);
//			f.dreheLinksZeit(1);
//		}
	}
	
	public void notsuicide() {
		
		f.fahreRueckwaertsStrecke(20);
		f.dreheRechtsGrad(90);
	}
	
	
	public void FahreRechts() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		LCD.drawString("Drehe rechts", 3, 4);
		LCD.refresh();
		
		Motor.A.setSpeed(360);
		Motor.B.setSpeed(360);
		
		Motor.C.forward();
		Motor.A.backward();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Motor.A.stop(true);
		Motor.B.stop();
	}
	
	public void FahreLinks() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		LCD.drawString("Drehe links", 3, 4);
		LCD.refresh();
		
		Motor.A.setSpeed(360);
		Motor.B.setSpeed(360);
		
		Motor.A.forward();
		Motor.B.backward();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Motor.A.stop(true);
		Motor.B.stop();
	}
	
	public void Vorsicht() {
		
		Motor.A.stop(true);
		Motor.B.stop();
	}

}
