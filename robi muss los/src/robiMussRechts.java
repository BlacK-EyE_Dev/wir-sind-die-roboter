import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.Motor;
import lejos.nxt.Sound;
import lejos.util.Delay;

public class robiMussRechts {
	
	public static void main(String[] args) {
	
	Sound.beep();
	 
	LCD.drawString("Eile!", 0, 0);
	 
	Button.ENTER.waitForPressAndRelease();
	 
	Delay.msDelay(1000);
	
	LCD.drawString("go go", 3, 4);
	LCD.refresh();
	
	Motor.A.setSpeed(355);
	Motor.B.setSpeed(355);
	
	Motor.A.forward();
	Motor.B.forward();
	
	try {
		Thread.sleep(3000);
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	Motor.A.stop(true);
	Motor.B.stop();
	
	methods.drehe_rechts();

}
}
