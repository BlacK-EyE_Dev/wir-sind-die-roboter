package Fahrzeug;

import lejos.nxt.NXTRegulatedMotor;
import lejos.nxt.LCD;
import lejos.nxt.Motor;

public class Fahrzeug implements FahrzeugInterface, AnzeigenInterface {

	final float DURCHMESSER_REIFEN = 5.6f;
	final float ABSTAND_ACHE = 11.3f;
	NXTRegulatedMotor rechterMotor = Motor.A;
	NXTRegulatedMotor linkerMotor = Motor.B;
	
	
	
	@Override
	public void zeigeText(String text) {
		// TODO Auto-generated method stub
		
		LCD.drawString(text, 2, 2);

	}

	@Override
	public void zeigeZahl(float zahl) {
		// TODO Auto-generated method stub
		

	}

	@Override
	public float getDurchmesserReifen() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getAbstandAchse() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getGeschwindigkeitLinks() {
		// TODO Auto-generated method stub
		
		return Motor.B.getSpeed();
	}

	@Override
	public int getGeschwindigkeitRechts() {
		// TODO Auto-generated method stub
		
		return Motor.A.getSpeed();
		
	}

	@Override
	public void setGeschwindigkeit(int links, int rechts) {
		// TODO Auto-generated method stub
		
		this.linkerMotor.setSpeed(links);
		this.rechterMotor.setSpeed(rechts);

	}

	@Override
	public void fahreVorwaerts() {
		
		this.linkerMotor.forward();
		this.rechterMotor.forward();

	}

	@Override
	public void fahreVorwaertsStrecke(int streckeInCm) {
		// TODO Auto-generated method stub
		
		double v = (this.getGeschwindigkeitLinks() + this.getGeschwindigkeitRechts())/2;
		
		double sad = (2 * Math.PI * this.DURCHMESSER_REIFEN/2)/12;
		
		double teiler = 360 / v;
		
		double zeit = ((streckeInCm / sad) * teiler);

		int time = (int) (zeit*100);
		
		this.fahreVorwaertsZeit(time);
		
	}

	@Override
	public void fahreVorwaertsZeit(int zeitInsek) {
		// TODO Auto-generated method stub
		
		this.fahreVorwaerts();
		
		try {
			Thread.sleep(zeitInsek);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.stoppe();

	}

	@Override
	public void fahreRueckwaerts() {
		// TODO Auto-generated method stub
		
		linkerMotor.backward();
		rechterMotor.backward();
		
	}

	@Override
	public void fahreRueckwaertsStrecke(int streckeInCm) {
		// TODO Auto-generated method stub
		
		double v = (this.getGeschwindigkeitLinks() + this.getGeschwindigkeitRechts())/2;
		
		double sad = (2 * Math.PI * this.DURCHMESSER_REIFEN/2)/11;
		
		double teiler = 360 / v;
		
		double zeit = ((streckeInCm / sad) * teiler);

		int time = (int) (zeit*100);
		
		this.fahreRueckwaertsZeit(time);
		
	}

	@Override
	public void fahreRueckwaertsZeit(int zeitInCM) {
		// TODO Auto-generated method stub
		
		this.fahreRueckwaerts();
		
		try {
			Thread.sleep(zeitInCM);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.stoppe();
		
	}

	@Override
	public void dreheRechts() {
		// TODO Auto-generated method stub
		
		Motor.C.forward();
		Motor.A.backward();

	}

	@Override
	public void dreheRechtsGrad(int winkel) {
		// TODO Auto-generated method stub
		
		this.rechterMotor.rotate((360 / 90) * winkel);
		this.stoppe();
	}
 
	@Override
	public void dreheRechtsZeit(int zeitInSek) {
		// TODO Auto-generated method stub
		
		try {
			Thread.sleep(zeitInSek * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		this.stoppe();

	}

	@Override
	public void dreheLinks() {
		
		Motor.A.forward();
		Motor.C.backward();
		
	}

	@Override
	public void dreheLinksGrad(int winkel) {
		// TODO Auto-generated method stub
		
		this.linkerMotor.rotate((360 / 90) * winkel);
		this.stoppe();
	}

	@Override
	public void dreheLinksZeit(int zeitInSek) {
		// TODO Auto-generated method stub
		
		this.dreheLinks();
		
		try {
			Thread.sleep(zeitInSek * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}

	@Override
	public void stoppe() {
		// TODO Auto-generated method stub
		
		Motor.A.stop(true);
		Motor.B.stop();

	}

}
