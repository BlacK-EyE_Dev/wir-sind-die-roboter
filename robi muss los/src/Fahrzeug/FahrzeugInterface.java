package Fahrzeug;

public interface FahrzeugInterface {
	
	float getDurchmesserReifen();
	float getAbstandAchse();
	int getGeschwindigkeitLinks();
	int getGeschwindigkeitRechts();
	void setGeschwindigkeit(int links, int rechts);
	void fahreVorwaerts();
	void fahreVorwaertsStrecke(int streckeInCm);
	void fahreVorwaertsZeit(int zeitInsek);
	void fahreRueckwaerts();
	void fahreRueckwaertsStrecke(int streckeInCm);
	void fahreRueckwaertsZeit(int zeitInCM);
	void dreheRechts();
	void dreheRechtsGrad(int winkel);
	void dreheRechtsZeit(int zeitInSek);
	void dreheLinks();
	void dreheLinksGrad(int winkel);
	void dreheLinksZeit(int zeitInSek);
	void stoppe();
}
