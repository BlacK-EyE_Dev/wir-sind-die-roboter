package Fahrzeug;

import lejos.nxt.LCD;
import lejos.nxt.Motor;

public class Varzoig implements FahrzeugInterface, AnzeigenInterface {

	@Override
	public void zeigeText(String text) {
		// TODO Auto-generated method stub
		
		LCD.drawString(text, 2, 2);

	}

	@Override
	public void zeigeZahl(float zahl) {
		// TODO Auto-generated method stub
		

	}

	@Override
	public float getDurchmesserReifen() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public float getAbstandAchse() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int getGeschwindigkeitLinks() {
		// TODO Auto-generated method stub
		
		Motor.B.getSpeed();
		return 0;
	}

	@Override
	public int getGeschwindigkeitRechts() {
		// TODO Auto-generated method stub
		
		Motor.A.getSpeed();
		return 0;
	}

	@Override
	public void setGeschwindigkeit(int links, int rechts) {
		// TODO Auto-generated method stub
		
		Motor.A.setSpeed(rechts);
		Motor.B.setSpeed(links);

	}

	@Override
	public void fahreVorwaerts() {
		// TODO Auto-generated method stub

	}

	@Override
	public void fahreVorwaertsStrecke(int streckeInCm) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fahreVorwaertsZeit(int zeitInsek) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fahreRueckwaerts() {
		// TODO Auto-generated method stub
		
		Motor.A.forward();
		Motor.B.forward();

	}

	@Override
	public void fahreRueckwaertsStrecke(int streckeInCm) {
		// TODO Auto-generated method stub

	}

	@Override
	public void fahreRueckwaertsZeit(int zeitInCM) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dreheRechts() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		
		Motor.C.forward();
		Motor.A.backward();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void dreheRechtsGrad(int winkel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dreheRechtsZeit(int zeitInSek) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dreheLinks() {
		// TODO Auto-generated method stub
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		
		Motor.A.forward();
		Motor.C.backward();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void dreheLinksGrad(int winkel) {
		// TODO Auto-generated method stub

	}

	@Override
	public void dreheLinksZeit(int zeitInSek) {
		// TODO Auto-generated method stub

	}

	@Override
	public void stoppe() {
		// TODO Auto-generated method stub
		
		Motor.A.stop(true);
		Motor.B.stop();

	}

}
