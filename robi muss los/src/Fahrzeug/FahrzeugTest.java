package Fahrzeug;

public class FahrzeugTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		 // Speed-Default-Wert ist 360
	    FahrzeugInterface einFahrzeug = new Fahrzeug();    
	    //Fahrzeugnamen �ndern
		
	    // 1. Aufgabe
	    for (int i = 1; i <= 2; i++) { 
	      einFahrzeug.fahreVorwaertsStrecke(50);
	      einFahrzeug.dreheRechtsGrad(90);
	      einFahrzeug.fahreVorwaertsStrecke(20);
	      einFahrzeug.dreheRechtsGrad(90);
	    }

	    // 2. Aufgabe
	    for (int i = 1; i <= 36; i++) {
	      einFahrzeug.fahreVorwaertsStrecke(5);
	      einFahrzeug.dreheLinksGrad(13);
	    }

	    // 3. Aufgabe
	    einFahrzeug.setGeschwindigkeit(600,600);
	    for (int i = 1; i <= 2; i++) {
	      einFahrzeug.fahreVorwaertsStrecke(50);
	      einFahrzeug.dreheRechtsGrad(90);
	      einFahrzeug.fahreVorwaertsStrecke(20);
	      einFahrzeug.dreheRechtsGrad(90);
	    }
	    
	    // 4. Aufgabe
	    // Fahrzeug f�hrt 10s vorw�rts, dabei "beschleunigt" es, 
	    // anschlie�end bleibt es 2s stehen und dann f�hrt es mit 
	    // kleinster Geschwindigkeit 5s r�ckw�rts	
	    
	    int x = 300;
	    
	    for (int i = 1; i <= 10; i++) {
	    	einFahrzeug.setGeschwindigkeit(x += 20, x+= 20);
	    	einFahrzeug.fahreVorwaerts();
	    }
	}

}
