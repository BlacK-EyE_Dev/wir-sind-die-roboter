package Fahrzeug;

public interface AnzeigenInterface {
	
	void zeigeText(String text);
	void zeigeZahl(float zahl);
}
