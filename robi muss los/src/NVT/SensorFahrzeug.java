package NVT;

import Fahrzeug.Fahrzeug;
import lejos.nxt.LightSensor;
import lejos.nxt.SensorPort;

public class SensorFahrzeug extends Fahrzeug implements LichtsensorInterface {
	
	SensorPort port1 = SensorPort.S1;
	private LightSensor lightSensor = new LightSensor(port1);
	private Object lichtSensor;

	public SensorFahrzeug(){
		
	}
	
	@Override
	public void kalibriereWerte() {
		// TODO Auto-generated method stub
		lightSensor.calibrateHigh();
		lightSensor.calibrateLow();

	}
	

	@Override
	public boolean isSchwarzerBereich() {
		// TODO Auto-generated method stub
		boolean isSchwarz = false;
		if (lightSensor.readValue() <= 40) {
			return true;
		}
		return false;
	}

	@Override
	public boolean isWeisserBereich() {
		// TODO Auto-generated method stub
		boolean isWeiss = false;
		if (lightSensor.readValue() >= 40) {
			return true;
		}
		return false;
		//is flasch
	}

	@Override
	public void lichtSensorEin() {
		// TODO Auto-generated method stub
		lightSensor.setFloodlight(true);

	}

	@Override
	public void lichtSensorAus() {
		// TODO Auto-generated method stub
		lightSensor.setFloodlight(false);

	}

}
