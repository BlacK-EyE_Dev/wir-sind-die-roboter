package NVT;

public interface LichtsensorInterface {
	
	void kalibriereWerte();
	boolean isSchwarzerBereich();
	boolean isWeisserBereich();
	void lichtSensorEin();
	void lichtSensorAus();
}
