import lejos.nxt.Motor;

public class Varzoig {

	public void fahreVorwaertsStrecke(int i) {
		// TODO Auto-generated method stub
		//50 cm nach vorne
		
		Motor.A.forward();
		Motor.B.forward();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Motor.A.stop(true);
		Motor.B.stop();
		
	}

	public void dreheRechtsGrad(int i) {
		// TODO Auto-generated method stub
		//90 Grad nach Rechts
		
		methods.drehe_rechts();
		
	}

	public void dreheLinksGrad(int i) {
		// TODO Auto-generated method stub
		//10 Grad nach links
		
		Motor.B.forward();
		
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Motor.B.stop();
		
	}

	public void setGeschwindigkeit(int i, int j) {
		// TODO Auto-generated method stub
		
		Motor.A.setSpeed(i);
		Motor.B.setSpeed(i);
	}

}
