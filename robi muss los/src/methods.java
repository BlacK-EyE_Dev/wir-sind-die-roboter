import lejos.nxt.LCD;
import lejos.nxt.Motor;

public class methods {

	public static void drehe_links() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		LCD.drawString("Drehe links", 3, 4);
		LCD.refresh();
		
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		
		Motor.A.forward();
		Motor.C.backward();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Motor.A.stop(true);
		Motor.C.stop();
	}
	
	
	public static void drehe_rechts() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		LCD.drawString("Drehe rechts", 3, 4);
		LCD.refresh();
		
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		
		Motor.C.forward();
		Motor.A.backward();
		
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Motor.A.stop(true);
		Motor.C.stop();
	}
	
	public static void fahre_Meter() {
		Motor.A.setSpeed(360);
		Motor.C.setSpeed(360);
		
		Motor.A.forward();
		Motor.C.forward();
		
		try {
			Thread.sleep(5862);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Motor.A.stop(true);
		Motor.C.stop();
	}
}
