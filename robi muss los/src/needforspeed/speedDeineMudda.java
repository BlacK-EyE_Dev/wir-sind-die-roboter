package needforspeed;

import Fahrzeug.Fahrzeug;
import Fahrzeug.FahrzeugInterface;
import NVT.SensorFahrzeug;
import lejos.nxt.Button;
import lejos.nxt.LCD;
import lejos.nxt.LightSensor;
import lejos.nxt.Motor;
import lejos.nxt.SensorPort;

public class speedDeineMudda {
	
	public static void dreheLinksAufStelle() {
		Motor.A.backward();
		Motor.B.forward();
	}
	
	public static void dreheRechtsAufStelle() {
		Motor.B.backward();
		Motor.A.forward();
	}
	
	public static void dreheLinksAufStelleGrad(int winkel) {
		Motor.A.backward();
		Motor.B.rotate((360/180)*winkel);
		Motor.A.stop(true);
		Motor.B.stop();
	}
	
	public static void dreheRechtsAufStelleGrad(int winkel) {
		Motor.B.backward();
		Motor.A.rotate((360/180)*winkel);
		Motor.A.stop(true);
		Motor.B.stop();
	}
	
	public static void main(String[] args) {
		SensorFahrzeug Leo = new SensorFahrzeug();
		Leo.setGeschwindigkeit(400, 400);
		LCD.drawString("Im not Alice", 0, 0);
		int value = 0;
		Button.ENTER.waitForPressAndRelease();
		Leo.fahreVorwaerts();
		
		while(true) {
			if(Leo.isWeisserBereich()) {
				int inkrement = 2;
				Leo.stoppe();
				Leo.setGeschwindigkeit(120, 120);
				while(Leo.isWeisserBereich()) {
					if(value == 0) {
						if(Leo.isWeisserBereich()) {
							dreheLinksAufStelle();
							for(int i = 0;i<inkrement;i++) {
								try {
									Thread.sleep(20);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							Leo.stoppe();
							inkrement += 2;
							
						}
						if(Leo.isWeisserBereich()) {
							dreheRechtsAufStelle();
							for(int i = 0;i<inkrement;i++) {
								try {
									Thread.sleep(20);
								} catch (InterruptedException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
							Leo.stoppe();
							inkrement += 2;
							
						}
					}
					
				}
				Leo.setGeschwindigkeit(360, 360);
				Leo.fahreVorwaerts();
			}
		}
	}

}
